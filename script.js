const tbody = document.querySelector('tbody');
const section = document.createElement('section');
section.classList.add('headers');
const h2 = document.createElement('h2');
h2.textContent = "Student Data";
section.appendChild(h2);
const btnElement = document.createElement('button');
btnElement.textContent = "Add Student";
section.appendChild(btnElement);
btnElement.classList.add('btn', 'add');
const containerSection = document.querySelector('.container');
const tableElement = document.querySelector('.table');
containerSection.insertBefore(section, tableElement);

//now insert thead elements from the table dynamically
const table = document.createElement('table');
const thead = document.createElement('thead');
thead.innerHTML = `<tr>
                            <th>studentId</th>
                            <th>firstName</th>
                            <th>lastName</th>
                            <th>age</th>
                            <th>qualification</th>
                            <th>mobileNumber</th>
                            <th>city</th>
                            <th></th>
                            <th></th>
                    </tr>`
tableElement.insertBefore(thead, tbody);


//for addstudents button apply functionalities
const addBtn = document.querySelector('.add');
addBtn.addEventListener("click", () => {
    form.classList.add('active');
    method = "POST"
})
const form = document.querySelector('.form-wrapper');
const cancelBtn = document.querySelector('.cancel');


//creating form elements dynamically
let formElement=document.createElement('form');
formElement.classList.add('studentForm');
let formEle=` 
                    <section class="group">
                        <label for="firstname">firstname</label>
                        <input type="text" class="form-control" id="firstname"  placeholder="Enter your firstname">
                    </section>

                    <section class="group">
                        <label for="lastname">lastname</label>
                        <input type="text" class="form-control" id="lastname" placeholder="Enter your lastname">
                    </section>

                    <section class="group">
                        <label for="age">age</label>
                        <input type="number" class="form-control" id="age" placeholder="Enter your age">
                    </section>

                    <section class="group">
                        <label for="Qualification">Qualification</label>
                        <input type="text" class="form-control" id="Qualification" placeholder="Enter your Qualification">
                    </section>

                    <section class="group">
                        <label for="mobileNumber">mobileNumber</label>
                        <input type="number" class="form-control" id="mobileNumber" placeholder="Enter your mobileNumber">
                    </section>

                    <section class="group">
                        <label for="city">City</label>
                        <input type="text" class="form-control" id="city" placeholder="Enter your city">
                    </section>

                    <section class="action">
                        <button class="btn btn-danger cancel">Cancel</button>
                        <button class="btn btn-primary save">Save changes</button>

                    </section> `
formElement.innerHTML=formEle;
let sectionForm=document.querySelector('.form-wrapper');
sectionForm.appendChild(formElement);





let students = [];
let id = null;
let getStudentDetails = async () => {
    try {
        const response = await fetch(("https://b955-103-51-153-190.in.ngrok.io/students"), {
            method: "GET",
            headers: new Headers({
                "ngrok-skip-browser-warning": '1234'
            }),
        })
        const data = await response.json();
        console.log(data);
        students = data;
        updateTable();
    } catch (error) {
        console.log("error");
    }
}
getStudentDetails();

//create a function for updateTable()
const updateTable = () => {
    console.log(students);
    let data = "";
    if (students.length > 0) {
        for (i = 0; i < students.length; i++) {
            console.log(students[i].studentId);
            data += `<tr id="${students[i].studentId}">
                         <td>${students[i].studentId}</td>
                         <td>${students[i].firstName}</td>
                         <td>${students[i].lastName}</td>
                         <td>${students[i].age}</td>
                         
                         <td>${students[i].qualification}</td>
                         <td>${students[i].mobileNumber}</td>
                         <td>${students[i].city}</td>

                         <td><button class="btn btn-primary" onclick="editStudent(event)">Edit</button></td>
                         <td><button class="btn btn-danger" onclick="deleteStudent(event)">Delete</button></td>
                    </tr>`
        }
        // now need to have when edit or delete button is pressesd then it need to show a pop window for that add click events in the buttons
        console.log(data);
        tbody.innerHTML = data;
    }
}



//create two functions for when edit button and delete button clicked
const editStudent = (e) => {
    form.classList.add('active');
    // httpMethod="PUT"
    console.log(e.target.parentElement.parentElement.id);
    id = e.target.parentElement.parentElement.id;
}


const deleteStudent = (e) => {
    console.log(e.target.parentElement.parentElement.id);
    id = e.target.parentElement.parentElement.id;
    fetch("https://b955-103-51-153-190.in.ngrok.io/students/${id}", { method: "DELETE" })
        .then(
            () => {
                getStudentDetails();
            }
        )
}



























